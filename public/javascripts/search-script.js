// Checks if all terms in search input match to a particular reference string
let allQueryTermsMatch = (queryTerms, queryIndex, reference) => {
  if (queryIndex < queryTerms.length) {
    if (reference.indexOf(queryTerms[queryIndex]) > -1) {
      return allQueryTermsMatch(queryTerms, queryIndex + 1, reference);
    } else {
        return false;
    }
  } else {
    return true;
  }
}

let onPageSearch = () => {
    // Declare variables
    let query = document.getElementById('search_input').value.toUpperCase();
    let cardElements = document.getElementsByClassName('col');

    /* Search the genome card collection by entire or partial strings found in
    that organism's common name, genus, or species, in any order. e.g. 'bidop ear'
    */
    [ ...cardElements].forEach(element => {
        let commonName = element.getElementsByTagName("h1")[0].innerText;
        let taxonomicName = element.getElementsByTagName("p")[0].innerText;
        let reference = commonName + taxonomicName;
        if (allQueryTermsMatch(query.split(' '), 0, reference.toUpperCase())) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
};

let clearSearch = () => {
    document.getElementById('search_input').value = '';
    onPageSearch();
};
