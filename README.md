# Genome Dashboard

Genome Dashboard is a web application (node.js) that shows a gallery of genome shortcuts for [Integrated Genome Browser](https://bioviz.org).

The Dashboard shows an image for every species and genome available from IGB's main Quickload site. 

Clicking a species image in the Dashboard tells IGB to open the latest genome for that species. (See Figure 1.)

Also, you can open earlier genomes, if available. (See Figure 2.)

For more information about using IGB, visit:

* [Users' Guide](https://wiki.transvar.org/display/igbman/Home)
* [IGB YouTube](https://www.youtube.com/channel/UC0DA2d3YdbQ55ljkRKHRBkg)

Figure 1. Genome Dashboard with genome shortcut images.
![Figure 1. Genome Dashboard with shortcut images](images/genome-dashboard-1.png)

Figure 2. Other genome versions available.
![Figure 1. Other genome versions available.](images/genome-dashboard-2.png)

* * *

## Quick start - running Genome Dashboard on your local computer

**Step 1.** Install `npm` (node package manager) and `nodejs`. 

On Ubuntu, install with:

```
sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm
```

On CentOS, install with:

```
sudo yum update
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install nodejs
```

**Step 2.** Install require plugins

Clone the repository, change into the project folder, and run the following command to install plugins required for the Genome Dashboard to run.

```
npm install
```

**Step 3.** Start the Genome Dashboard application

Change into the project folder and run:


```
node ./bin/www
```

Alternatively, if you have `nodemon` installed, run the following command:

```
nodemon
```

**Step 4.** Download and start IGB

Download and run the installer for your platform from [Bioviz.org](https://bioviz.org). 

**Step 5.** Open Genome Dashboard

Open [localhost:3000](http://localhost:3000) in your Web browser and click and image to load the genome into IGB.

* * *
### Setting up on local development Environment using PyCharm IDE

**Step 1.** Install `npm` (node package manager) and `nodejs`.

Using homebrew via terminal.
```
brew update
brew install node
```
Alternatively, Using Package installer:

Go to [https://nodejs.org/en/download/](https://nodejs.org/en/download/) and download th pkg for nodejs.

**Step 2.** Verify installation by checking the version for node and npm.

Check node version:
```
node -v
```
Check npm version:
```
npm -v
```

**Step 3.** Install NodeJs plugin for PyCharm.

Default PyCharm does not come in built with NodeJs plugin.

```PyCharm | Preferences... | Plugin | Search Node.js | install plugin```

**Step 4.** Clone the repository, Import/Open the project folder, and run the following command in the terminal 
of the PyCharm to install plugins required for the Genome Dashboard to run.

```
npm install
```
**Step 5.** Setup execution configuration to Run the project.

```Run | Edit Configuration/Add Configuration```

Click the ```+``` button in the Run/Debug Configuration window. 

Select ```Node.js``` give a name to the config.

Change the ```JavaScript file``` to ```./bin/www```

```Working directory``` is the project directory repo.

```Node Interpreter```  is installed node version.

**Step 6.** Run the project.

Select the configuration just created above from drop down and click play button.


* * *

To deploy on AWS, see:

* https://bitbucket.org/lorainelab/bioviz-playbooks.


* * *

## Credits

* Code and design: [Sameer Shanbhag](mailto:sshanbh1@uncc.edu), [Chester Dias](mailto:cdias1@uncc.edu), [Philip Badzuh](pbadzuh@uncc.edu)
* Concept: Nowlan Freese and Ann Loraine