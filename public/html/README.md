During deployment, two files will be copied from bioviz repository, edited, and
replace the ones shown below:

* ./menu.html
* ../stylesheets/menu.css

This is ensure that genome dashboard app will show the same menu as the
rest of the bioviz site. During development on your local machine, the menu may
appear differently from the one on bioviz.org, since the files above to be
overwritten during deployment may be outdated. If changes need to be made to the
menu, apply them to the bioviz repository, not here.  
